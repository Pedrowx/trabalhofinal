#include <stdio.h>
#include "mpi.h"

void bs(int n, int * vetor)
{
    int c=0, d, troca, trocou =1;

    while (c < (n-1) & trocou )
        {
        trocou = 0;
        for (d = 0 ; d < n - c - 1; d++)
            if (vetor[d] > vetor[d+1])
                {
                troca      = vetor[d];
                vetor[d]   = vetor[d+1];
                vetor[d+1] = troca;
                trocou = 1;
                }
        c++;
        }
}

void fillWorstCase(int vetSize, int * vet)
{
    for(int i = 0; i < vetSize; i++){
        vet[i] = vetSize - i;
    }
}

int main(int argc, char** argv)
  {  
  int VetSize = 1000000;   
  int vet[VetSize]; 
  int sliceSize = 10;    
  int sliceVetSize;   
  int lastElementAux;       
  int isOrdered;         
  int replaceResult;    

  int my_rank;       
  int num_proc;          
  MPI_Status status;  

  MPI_Init(&argc , &argv); 
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
  MPI_Comm_size(MPI_COMM_WORLD, &num_proc); 

  isOrdered = 0;
  sliceVetSize = VetSize/num_proc;
  int vet_aux[sliceVetSize];   
  int troca[num_proc];
  int aux1[(sliceVetSize/sliceSize)]; 
  int aux2[(sliceVetSize/sliceSize)];   

  // Processo de inicializacao
  if ( my_rank == 0 ){
    fillWorstCase(VetSize,vet);

    int counter= sliceVetSize;
    int l;
    for(int i = 1; i < num_proc; i++){
        l = 0;
        for(int j = counter; j < (counter+ sliceVetSize); j++){
            vet_aux[l] = vet[j];
            l++;
        }
        counter= counter+ sliceVetSize;
        MPI_Send(&vet_aux, sliceVetSize, MPI_INT, i, 1, MPI_COMM_WORLD); 
    }
  }else
    MPI_Recv(&vet_aux, sliceVetSize, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    if(my_rank == 0){
        for(int i = 0; i < sliceVetSize; i++){
            vet_aux[i] = vet[i];
        }
    }
          
    // Inicia o processamento das trocas
    while(isOrdered == 0){
        bs(sliceVetSize,vet_aux);
        troca[my_rank] = 0; 

        // Se eu nao for lastElementAux eu mando para meu vizinho da direita
        if(my_rank != (num_proc-1))                         
            MPI_Send(&vet_aux[sliceVetSize - 1], 1, MPI_INT, (my_rank+1), 1, MPI_COMM_WORLD); 
        if(my_rank != 0){
            MPI_Recv(&lastElementAux, 1, MPI_INT, (my_rank -1), MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            // Tem que trocar?
            if(lastElementAux > vet_aux[0]){
                troca[my_rank] = 1;
            }else{         
                troca[my_rank] = 0;
            }
        }

        for(int i = 0; i < num_proc; i++)
            MPI_Bcast(&troca[i], 1, MPI_INT, i, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);

        // Imprimo o vet de Troca 
        replaceResult = 0;
        for(int i = 0; i < num_proc; i++){
            replaceResult = replaceResult + troca[i];
        }
        if(replaceResult > 0){
            isOrdered = 0;
        }else{
            isOrdered = 1;
        }

        // Realiza as trocas entre os vetores
        if(troca[my_rank] == 1 && my_rank > 0){
            for(int i = 0; i < (sliceVetSize/sliceSize);i++){
                aux1[i] = vet_aux[i];
            }
            MPI_Send(&aux1, (sliceVetSize/sliceSize), MPI_INT, (my_rank-1), 1, MPI_COMM_WORLD); 
            MPI_Recv(&aux2, (sliceVetSize/sliceSize), MPI_INT, (my_rank-1), MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            for(int i = 0; i < (sliceVetSize/sliceSize);i++){
                vet_aux[i] = aux2[i];
            }
        }

        if(troca[my_rank + 1] == 1 && my_rank < (num_proc-1)){
            MPI_Recv(&aux1, (sliceVetSize/sliceSize), MPI_INT, (my_rank+1), MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            int x = 0;
            for(int i = sliceVetSize - (sliceVetSize/sliceSize); i < sliceVetSize;i++){
                aux2[x] = vet_aux[i];
                x= x + 1;
            }
            int y = 0;
            for(int i = sliceVetSize - (sliceVetSize/sliceSize); i < sliceVetSize;i++){
                vet_aux[i] = aux1[y];
                y = y + 1;
            }
            MPI_Send(&aux2, (sliceVetSize/sliceSize), MPI_INT, (my_rank+1), 1, MPI_COMM_WORLD); 
        }
    } 

  // Imprime a primeria parte do resultado final ordenado
  if(my_rank == 0) {
    printf("Primeira parte do vet final:\n");
    for(int i = 0; i < sliceVetSize; i++){
        printf(" %d, ", vet_aux[i]); 
    }
    printf(" \n");
  }

  MPI_Finalize();
}